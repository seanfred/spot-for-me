/**
 * Created by seanfred on 6/9/15.
 */
'use strict';

angular.module('spotForMe')
  .controller('QuestionCtrl', function ($scope) {
    $scope.answerQuestions = [];
    $scope.values = [];

    $scope.changeInput = function(input) {
      var question = input.$parent.question;
          question.userAnswer = input.answer;
      if($scope.answerQuestions.indexOf(question) != -1){
        //question has already been answered and pushed
        var index = $scope.answerQuestions.indexOf(question);
        $scope.answerQuestions.splice(index, 1, question);
      }else{
        $scope.answerQuestions.push(question);
      }
    };



    $scope.addDestination = function(){
      $scope.values = [];
      var value = this.answer.value;
      $scope.values.push(value);
    };

    $scope.questions = [
      {
        'question': 'If you could live in another time it would be?',
        'answers': [
          {
            'answer': 'The Wild, Wild West',
            'group': 1
          },
          {
            'answer': 'Knights in Shining Armour',
            'group': 3
          },
          {
            'answer': 'The Roaring 20s',
            'group': 5
          },
          {
            'answer': 'I live for now, let the past be the past.',
            'group': 4
          }
        ]
      },
      {
        'question': 'In 10 years I will be...',
        'answers': [
          {
            'answer': 'In the same job with increased pay for my loyalty',
            'group': 1
          },
          {
            'answer': 'With the same company but will be up a few rungs on the ladder',
            'group': 2
          },
          {
            'answer': 'In a completely different job/career. Change is good.',
            'group': 3
          },
          {
            'answer': 'Me? A job?',
            'group': 4
          }
        ]
      },
      {
        'question': 'What ice cream do you like best?',
        'answers': [
          {
            'answer': 'Rocky Road',
            'group': 1
          },
          {
            'answer': 'Vanilla',
            'group': 2
          },
          {
            'answer': 'Maple Nut',
            'group': 3
          },
          {
            'answer': 'Mango',
            'group': 4
          }
        ]
      },
      {
        'question': 'You are at your favorite club. What music is playing?',
        'answers': [
          {
            'answer': 'Jazz',
            'group': 2
          },
          {
            'answer': 'Classic Rock',
            'group': 1
          },
          {
            'answer': 'Hip Hop',
            'group': 3
          },
          {
            'answer': 'Pop',
            'group': 4
          }
        ]
      },
      {
        'question': 'You have a choice, it would be...',
        'answers': [
          {
            'answer': 'Smart Car',
            'group': 1
          },
          {
            'answer': '1969 Chevy Camaro SS, completely restored',
            'group': 5
          },
          {
            'answer': 'Toyota Prius',
            'group': 3
          },
          {
            'answer': 'Ford F-150 Platinum',
            'group': 4
          }
        ]
      },
      {
        'question': 'What is your favorite color?',
        'answers': [
          {
            'answer': 'Red',
            'group': 2
          },
          {
            'answer': 'Blue',
            'group': 1
          },
          {
            'answer': 'Purple',
            'group': 5
          },
          {
            'answer': 'Black',
            'group': 3
          }
        ]
      },
      {
        'question': 'You are planning a long weekend with your other half. You would...',
        'answers': [
          {
            'answer': 'Rent a small remote cabin.',
            'group': 1
          },
          {
            'answer': 'Invite a group of your closest friends.',
            'group': 3
          },
          {
            'answer': 'Who plans? Jump in the car and see where we end up.',
            'group': 5
          },
          {
            'answer': 'Only the finest food and drink for my love.',
            'group': 2
          }
        ]
      },
      {
        'question': 'You grab a quick lunch during work. What is it?',
        'answers': [
          {
            'answer': 'Soup or Salad',
            'group': 3
          },
          {
            'answer': 'Cheeseburger & Fries',
            'group': 1
          },
          {
            'answer': 'Leftovers from the evening before',
            'group': 2
          },
          {
            'answer': 'Something from the vending machine',
            'group': 4
          }
        ]
      },
      {
        'question': 'A movie you like is?',
        'answers': [
          {
            'answer': 'The Hangover',
            'group': 5
          },
          {
            'answer': 'Marley & Me',
            'group': 1
          },
          {
            'answer': 'Titanic',
            'group': 2
          },
          {
            'answer': 'Star Wars – All of Them!',
            'group': 3
          }
        ]
      },
      {
        'question': 'What quality do you look for most in a friend?',
        'answers': [
          {
            'answer': 'Sense of Humor',
            'group': 5
          },
          {
            'answer': 'Ethical',
            'group': 2
          },
          {
            'answer': 'Positive Attitude',
            'group': 3
          },
          {
            'answer': 'Good Listener',
            'group': 1
          }
        ]
      },
      {
        'question': 'What do you find the most beautiful?',
        'answers': [
          {
            'answer': 'Sunsets',
            'group': 4
          },
          {
            'answer': 'Manicured Gardens',
            'group': 2
          },
          {
            'answer': 'Sunrises',
            'group': 1
          },
          {
            'answer': 'Snow-peaked Mountains',
            'group': 3
          }
        ]
      },
      {
        'question': ' I am most comfortable in...',
        'answers': [
          {
            'answer': 'Flip Flops',
            'group': 4
          },
          {
            'answer': 'Boots',
            'group': 1
          },
          {
            'answer': 'Sneakers',
            'group': 2
          },
          {
            'answer': 'I go without shoes when I can.',
            'group': 5
          }
        ]
      }
    ];
  });
