'use strict';

angular.module('spotForMe')
  .controller('DestinationCtrl', function ($scope) {
    $scope.destinations = [
      {
        'title': 'Ixtapa-Zihuatanejo, Guerrero, Mexico',
        'description': 'HTML enhanced for web apps!',
        'logo': 'angular.png',
        'number': 1
      },
      {
        'title': "New Zealand's South Island",
        'description': 'Time-saving synchronised browser testing.',
        'logo': 'browsersync.png',
        'number': 2
      },
      {
        'title': "Utah's National Parks",
        'description': 'The streaming build system.',
        'logo': 'gulp.png',
        'number': 3
      },
      {
        'title': 'The Alaska Highway – One of the greatest North American Road Trips',
        'url': 'http://jasmine.github.io/',
        'description': 'Behavior-Driven JavaScript.',
        'logo': 'jasmine.png',
      },
      {
        'title': 'Transylvania, Romania',
        'description': 'Spectacular Test Runner for JavaScript.',
        'logo': 'karma.png'
      },
      {
        'title': 'Dublin, Ireland',
        'description': 'End to end test framework for AngularJS applications built on top of WebDriverJS.',
        'logo': 'protractor.png'
      },
      {
        'title': 'Kananaskis Country, Alberta, Canada',
        'description': 'Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web.',
        'logo': 'bootstrap.png'
      },
      {
        'title': 'French Quarter, New Orleans, Louisiana',
        'description': 'Bootstrap components written in pure AngularJS by the AngularUI Team.',
        'logo': 'ui-bootstrap.png'
      },
      {
        'title': 'Cape Town, South Africa',
        'description': 'Node.js binding to libsass, the C version of the popular stylesheet preprocessor, Sass.',
        'logo': 'node-sass.png'
      },
      {
        'title': 'Buenos Aires, Argentina',
        'description': 'Node.js binding to libsass, the C version of the popular stylesheet preprocessor, Sass.',
        'logo': 'node-sass.png'
      }
    ];
  });
