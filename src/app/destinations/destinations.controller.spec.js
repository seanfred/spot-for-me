'use strict';

describe('controllers', function(){
  var scope;

  beforeEach(module('spotForMe'));

  beforeEach(inject(function($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should define more than 5 awesome destinations', inject(function($controller) {
    expect(scope.destinations).toBeUndefined();

    $controller('DestinationCtrl', {
      $scope: scope
    });

    expect(angular.isArray(scope.destinations)).toBeTruthy();
    expect(scope.destinations.length > 5).toBeTruthy();
  }));
});
