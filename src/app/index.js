'use strict';

angular.module('spotForMe', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'restangular', 'ui.router', 'ui.bootstrap'])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('destinations', {
        url: '/',
        templateUrl: 'app/destinations/destinations.html',
        controller: 'DestinationCtrl'
      })
      .state('questionss', {
        url: '/questions',
        templateUrl: 'app/questions/questions.html',
        controller: 'QuestionCtrl'
      });

    $urlRouterProvider.otherwise('/');
  })
;
